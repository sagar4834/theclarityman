<?php include 'session.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>The Clarity Man</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
  <link rel="stylesheet" href="../vendors/css/vendor.addons.css">
  <!-- endinject -->
  <!-- vendor css for this page -->
  <link rel="stylesheet" href="../vendors/iconfonts/flag-icon-../css/../css/flag-icon.min.css">
  <!-- End vendor css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../images/favicon.ico">
</head>
<body class="header-fixed">
  <!-- partial:partials/_header.html -->
  <!-- Global site tag (gtag.js) - Google Analytics -->


  <nav class="t-header">
    <div class="t-header-brand-wrapper">
      <a href="index.html">
        <img class="logo" src="../images/logo.png" alt="">
        <img class="logo-mini" src="images/logo_mini.svg" alt="">
      </a>
      <button class="t-header-toggler t-header-desk-toggler d-none d-lg-block">
        <svg class="logo" viewBox="0 0 200 200">
          <path class="top" d="
          M 40, 80
          C 40, 80 120, 80 140, 80
          C180, 80 180, 20  90, 80
          C 60,100  30,120  30,120
          "></path>
          <path class="middle" d="
          M 40,100
          L140,100
          "></path>
          <path class="bottom" d="
          M 40,120
          C 40,120 120,120 140,120
          C180,120 180,180  90,120
          C 60,100  30, 80  30, 80
          "></path>
        </svg>
      </button>
    </div>
    <div class="t-header-content-wrapper">
      <div class="t-header-content">
        <button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none">
          <i class="mdi mdi-menu"></i>
        </button>
      </div>
    </div>
  </nav>
  <!-- partial -->
  <div class="page-body">
    <!-- partial:partials/_sidebar.html -->
    <?php include 'header.php' ?>
    <!-- partial -->






    <div class="page-content-wrapper">


      <div class="page-content-wrapper-inner">
        <div class="viewport-header">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item">
                <a href="#">Subscribers</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">All Subscribers</li>
            </ol>
          </nav>
        </div>
        <div class="content-viewport">
          <div class="row">

            <div class="col-lg-12">
              <div class="grid">
                <p class="grid-header">All Subscribers</p>
                <div class="item-wrapper">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Mobile</th>
                          <th>Email</th>
                          <th>Profession</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>
                        <tr>
                          <td class="d-flex align-items-center border-top-0">
                            <span>Sagar Pawar</span>
                          </td>
                          <td>9004998995</td>
                          <td>sagar.pawar68@gmail.com</td>
                          <td>Web Designer</td>
                        </tr>




                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



      <!-- content viewport ends -->
      <!-- partial:partials/_footer.html -->
      <?php include 'footer.php' ?>
      <!-- partial -->
    </div>





    <!-- page content ends -->
  </div>
  <!--page body ends -->
  <!-- SCRIPT LOADING START FORM HERE /////////////-->
  <!-- plugins:js -->
  <script src="../vendors/js/core.js"></script>
  <script src="../vendors/js/vendor.addons.js"></script>
  <!-- endinject -->
  <!-- Vendor Js For This Page Ends-->
  <script src="../vendors/chartjs/Chart.min.js"></script>
  <!-- Vendor Js For This Page Ends-->
  <script src="../js/script.js"></script>
</body>
</html>