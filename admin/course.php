<?php include 'session.php'; ?>
<?php include 'var.php'; ?>
<?php $course_id = $_GET['id'] ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>The Clarity Man</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
  <link rel="stylesheet" href="../vendors/css/vendor.addons.css">
  <!-- endinject -->
  <!-- vendor css for this page -->
  <link rel="stylesheet" href="../vendors/iconfonts/flag-icon-../css/../css/flag-icon.min.css">
  <!-- End vendor css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../images/favicon.ico">
</head>
<body class="header-fixed">
  <!-- partial:partials/_header.html -->
  <!-- Global site tag (gtag.js) - Google Analytics -->


  <nav class="t-header">
    <div class="t-header-brand-wrapper">
      <a href="index.html">
        <img class="logo" src="../images/logo.png" alt="">
        <img class="logo-mini" src="images/logo_mini.svg" alt="">
      </a>
      <button class="t-header-toggler t-header-desk-toggler d-none d-lg-block">
        <svg class="logo" viewBox="0 0 200 200">
          <path class="top" d="
          M 40, 80
          C 40, 80 120, 80 140, 80
          C180, 80 180, 20  90, 80
          C 60,100  30,120  30,120
          "></path>
          <path class="middle" d="
          M 40,100
          L140,100
          "></path>
          <path class="bottom" d="
          M 40,120
          C 40,120 120,120 140,120
          C180,120 180,180  90,120
          C 60,100  30, 80  30, 80
          "></path>
        </svg>
      </button>
    </div>
    <div class="t-header-content-wrapper">
      <div class="t-header-content">
        <button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none">
          <i class="mdi mdi-menu"></i>
        </button>
      </div>
    </div>
  </nav>
  <!-- partial -->
  <div class="page-body">
    <!-- partial:partials/_sidebar.html -->
    <?php include 'header.php' ?>
    <!-- partial -->


    



    <div class="page-content-wrapper">

      <?php 

      $username = $_SESSION['username'];
      $get_admin_user_data = "SELECT id FROM admin_user_data WHERE username = $username";
      $run=mysqli_query($mysqli,$get_admin_user_data); 
      $fetch_course = "SELECT * FROM courses_list WHERE id = '$course_id'";
      $run = mysqli_query($mysqli,$fetch_course);
      $row=$run->fetch_array();

      ?>




      <div class="page-content-wrapper-inner">
        <div class="viewport-header">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
              <li class="breadcrumb-item">
                <a href="dashboard.php">Dashboard</a>
              </li>
              <li class="breadcrumb-item">
                <a href="javascript:void(0)"><?php echo $row['category'] ?></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $row['title'] ?></li>
            </ol>
          </nav>
        </div>




        <div class="content-viewport">
          <div class="row">
            <div class="col-lg-12">
              <div class="grid">
                <div class="grid-body">


                  <div class="row">
                    <h1 class="mb-4"><?php echo $row['title'] ?></h1>
                  </div>



                  <div class="row">
                    <div class="col-md-4">
                      <img class="img-responsive" src="upload_image/<?php echo $row['image1'] ?>" alt="">
                    </div>

                    <div class="col-md-4">
                      <img class="img-responsive" src="upload_image/<?php echo $row['image2'] ?>" alt="">
                    </div>

                    <div class="col-md-4">
                      <img class="img-responsive" src="upload_image/<?php echo $row['image3'] ?>" alt="">
                    </div>
                  </div>


                  <br>


                  <div class="row">
                    <div class="col-md-4">
                      <iframe width="100%" height="180" 
                      src="<?php echo $row['video1'] ?>"
                      frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    <div class="col-md-4">
                      <iframe width="100%" height="180" 
                      src="<?php echo $row['video2'] ?>"
                      frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    <div class="col-md-4">
                      <iframe width="100%" height="180" 
                      src="<?php echo $row['video3'] ?>"
                      frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                  </div>



                  


                  <p class="mb-3">
                    <?php echo $row['description'] ?>
                  </p>





                  <div class="xxx mb-2">
                    <a target="_blank" href="upload_pdf/<?php echo $row['pdf1'] ?>" class="btn btn-sm btn-primary" type="submit">Download PDF</a>

                    <a target="_blank" href="upload_pdf/<?php echo $row['pdf2'] ?>" class="btn btn-sm btn-primary" type="submit">Download PDF</a>

                    <a target="_blank" href="upload_pdf/<?php echo $row['pdf3'] ?>" class="btn btn-sm btn-primary" type="submit">Download PDF</a>


                  </div>






                </div>
              </div>
            </div>    
          </div>
        </div>
      </div>



      <!-- content viewport ends -->
      <!-- partial:partials/_footer.html -->
      <?php include 'footer.php' ?>
      <!-- partial -->
    </div>





    <!-- page content ends -->
  </div>
  <!--page body ends -->
  <!-- SCRIPT LOADING START FORM HERE /////////////-->
  <!-- plugins:js -->
  <script src="../vendors/js/core.js"></script>
  <script src="../vendors/js/vendor.addons.js"></script>
  <!-- endinject -->
  <!-- Vendor Js For This Page Ends-->
  <script src="../vendors/chartjs/Chart.min.js"></script>
  <!-- Vendor Js For This Page Ends-->
  <script src="../js/script.js"></script>
</body>
</html>