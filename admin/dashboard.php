<?php include 'session.php'; ?>
<?php include 'var.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  <title>The Clarity Man</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
  <link rel="stylesheet" href="../vendors/css/vendor.addons.css">
  <!-- endinject -->
  <!-- vendor css for this page -->
  <link rel="stylesheet" href="../vendors/iconfonts/flag-icon-../css/../css/flag-icon.min.css">
  <!-- End vendor css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../images/favicon.ico">
</head>
<body class="header-fixed">
  <!-- partial:partials/_header.html -->
  <!-- Global site tag (gtag.js) - Google Analytics -->


  <nav class="t-header">
    <div class="t-header-brand-wrapper">
      <a href="index.html">
        <img class="logo" src="../images/logo.png" alt="">
        <img class="logo-mini" src="images/logo_mini.svg" alt="">
      </a>
      <button class="t-header-toggler t-header-desk-toggler d-none d-lg-block">
        <svg class="logo" viewBox="0 0 200 200">
          <path class="top" d="
          M 40, 80
          C 40, 80 120, 80 140, 80
          C180, 80 180, 20  90, 80
          C 60,100  30,120  30,120
          "></path>
          <path class="middle" d="
          M 40,100
          L140,100
          "></path>
          <path class="bottom" d="
          M 40,120
          C 40,120 120,120 140,120
          C180,120 180,180  90,120
          C 60,100  30, 80  30, 80
          "></path>
        </svg>
      </button>
    </div>
    <div class="t-header-content-wrapper">
      <div class="t-header-content">
        <button class="t-header-toggler t-header-mobile-toggler d-block d-lg-none">
          <i class="mdi mdi-menu"></i>
        </button>
      </div>
    </div>
  </nav>
  <!-- partial -->
  <div class="page-body">
    <!-- partial:partials/_sidebar.html -->
    <?php include 'header.php' ?>
    <!-- partial -->






    <div class="page-content-wrapper">
      <div class="page-content-wrapper-inner">
        <div class="viewport-header">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb has-arrow">
              <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">Overview</li>
            </ol>
          </nav>
        </div>





        <div class="content-viewport">
         <!--  <div class="row">
            <div class="col-md-7 equel-grid order-md-2">
              <div class="grid d-flex flex-column justify-content-between overflow-hidden">
                <div class="grid-body">
                  <div class="d-flex justify-content-between">
                    <p class="card-title">Sales Revenue</p>
                    <div class="chartjs-legend" id="sales-revenue-chart-legend"></div>
                  </div>
                  <div class="d-flex">
                    <p class="d-none d-xl-block">12.5% Growth compared to the last week</p>
                    <div class="ml-auto">
                      <h2 class="font-weight-medium text-gray">
                        <i class="mdi mdi-menu-up text-success"></i>
                        <span class="animated-count">25.04</span>%
                      </h2>
                    </div>
                  </div>
                </div>
                <canvas class="mt-4" id="sales-revenue-chart" height="245"></canvas>
              </div>
            </div>
            <div class="col-md-5 order-md-0">
              <div class="row">
                <div class="col-6 equel-grid">
                  <div class="grid d-flex flex-column align-items-center justify-content-center">
                    <div class="grid-body text-center">
                      <div class="profile-img img-rounded bg-inverse-primary no-avatar component-flat mx-auto mb-4">
                        <i class="mdi mdi-account-group mdi-2x"></i>
                      </div>
                      <h2 class="font-weight-medium">
                        <span class="animated-count">21.2</span>k
                      </h2>
                      <small class="text-gray d-block mt-3">Total Followers</small>
                      <small class="font-weight-medium text-success">
                        <i class="mdi mdi-menu-up"></i>
                        <span class="animated-count">12.01</span>%
                      </small>
                    </div>
                  </div>
                </div>
                <div class="col-6 equel-grid">
                  <div class="grid d-flex flex-column align-items-center justify-content-center">
                    <div class="grid-body text-center">
                      <div class="profile-img img-rounded bg-inverse-danger no-avatar component-flat mx-auto mb-4">
                        <i class="mdi mdi-airballoon mdi-2x"></i>
                      </div>
                      <h2 class="font-weight-medium">
                        <span class="animated-count">1.6</span>k
                      </h2>
                      <small class="text-gray d-block mt-3">Impression</small>
                      <small class="font-weight-medium text-danger">
                        <i class="mdi mdi-menu-down"></i>
                        <span class="animated-count">03.45</span>%
                      </small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-6 equel-grid">
                  <div class="grid d-flex flex-column align-items-center justify-content-center">
                    <div class="grid-body text-center">
                      <div class="profile-img img-rounded bg-inverse-warning no-avatar component-flat mx-auto mb-4">
                        <i class="mdi mdi-fire mdi-2x"></i>
                      </div>
                      <h2 class="font-weight-medium animated-count">2363</h2>
                      <small class="text-gray d-block mt-3">Reach</small>
                      <small class="font-weight-medium text-danger">
                        <i class="mdi mdi-menu-down"></i>
                        <span class="animated-count">12.15</span>%
                      </small>
                    </div>
                  </div>
                </div>
                <div class="col-6 equel-grid">
                  <div class="grid d-flex flex-column align-items-center justify-content-center">
                    <div class="grid-body text-center">
                      <div class="profile-img img-rounded bg-inverse-success no-avatar component-flat mx-auto mb-4">
                        <i class="mdi mdi-charity mdi-2x"></i>
                      </div>
                      <h2 class="font-weight-medium">
                        <span class="animated-count">23.6</span>%
                      </h2>
                      <small class="text-gray d-block mt-3">Engagement Rate</small>
                      <small class="font-weight-medium text-success">
                        <i class="mdi mdi-menu-up"></i>
                        <span class="animated-count">51.03</span>%
                      </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8 equel-grid">
              <div class="grid">
                <div class="grid-body py-3">
                  <p class="card-title ml-n1">Order History</p>
                </div>
                <div class="table-responsive">
                  <table class="table table-sm">
                    <thead>
                      <tr class="solid-header">
                        <th colspan="2" class="pl-4">Customer</th>
                        <th>Order No</th>
                        <th>Purchased On</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="pr-0 pl-4">
                          <img class="profile-img img-sm" src="images/profile/male/image_4.png" alt="profile image">
                        </td>
                        <td class="pl-md-0">
                          <small class="text-black font-weight-medium d-block">Barbara Curtis</small>
                          <span>
                            <span class="status-indicator rounded-indicator small bg-primary"></span>Account Deactivated
                          </span>
                        </td>
                        <td>
                          <small>8523537435</small>
                        </td>
                        <td>Just Now</td>
                      </tr>
                      <tr>
                        <td class="pr-0 pl-4">
                          <img class="profile-img img-sm" src="images/profile/male/image_3.png" alt="profile image">
                        </td>
                        <td class="pl-md-0">
                          <small class="text-black font-weight-medium d-block">Charlie Hawkins</small>
                          <span>
                            <span class="status-indicator rounded-indicator small bg-success"></span>Email Verified
                          </span>
                        </td>
                        <td>
                          <small>9537537436</small>
                        </td>
                        <td>Mar 04, 2018 11:37am</td>
                      </tr>
                      <tr>
                        <td class="pr-0 pl-4">
                          <img class="profile-img img-sm" src="images/profile/female/image_2.png" alt="profile image">
                        </td>
                        <td class="pl-md-0">
                          <small class="text-black font-weight-medium d-block">Nina Bates</small>
                          <span>
                            <span class="status-indicator rounded-indicator small bg-warning"></span>Payment On Hold
                          </span>
                        </td>
                        <td>
                          <small>7533567437</small>
                        </td>
                        <td>Mar 13, 2018 9:41am</td>
                      </tr>
                      <tr>
                        <td class="pr-0 pl-4">
                          <img class="profile-img img-sm" src="images/profile/male/image_10.png" alt="profile image">
                        </td>
                        <td class="pl-md-0">
                          <small class="text-black font-weight-medium d-block">Hester Richards</small>
                          <span>
                            <span class="status-indicator rounded-indicator small bg-success"></span>Email Verified
                          </span>
                        </td>
                        <td>
                          <small>5673467743</small>
                        </td>
                        <td>Feb 21, 2018 8:34am</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <a class="border-top px-3 py-2 d-block text-gray" href="#">
                  <small class="font-weight-medium">
                    <i class="mdi mdi-chevron-down mr-2"></i>View All Order History
                  </small>
                </a>
              </div>
            </div>
            <div class="col-md-4 equel-grid">
              <div class="row flex-grow">
                <div class="col-12 equel-grid">
                  <div class="grid widget-revenue-card">
                    <div class="grid-body d-flex flex-column h-100">
                      <div class="split-header">
                        <p class="card-title">Server Load</p>
                        <div class="content-wrapper v-centered">
                          <small class="text-muted">2h ago</small>
                          <span class="btn action-btn btn-refresh btn-xs component-flat">
                            <i class="mdi mdi-autorenew"></i>
                          </span>
                        </div>
                      </div>
                      <div class="mt-auto">
                        <h3 class="font-weight-medium mt-2">69.05%</h3>
                        <p class="text-gray">Storage is getting full</p>
                        <div class="d-flex justify-content-between text-muted mt-3">
                          <small>Usage</small>
                          <small>35.62 GB / 2 TB</small>
                        </div>
                        <div class="progress progress-slim mt-2">
                          <div class="progress-bar bg-primary" role="progressbar" style="width: 68%" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 equel-grid">
                  <div class="grid widget-sales-card d-flex flex-column">
                    <div class="grid-body pb-3">
                      <div class="wrapper d-flex">
                        <p class="card-title">Performance</p>
                        <div class="badge badge-success ml-auto">+ 12.42%</div>
                      </div>
                      <div class="wrapper mt-2">
                        <h3>321,212</h3>
                        <small class="text-gray">More traffic in this week</small>
                      </div>
                    </div>
                    <div class="mt-auto">
                      <canvas class="w-100" id="sales-conversion" height="70"></canvas>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          
        </div>



      </div>
      <!-- content viewport ends -->
      <!-- partial:partials/_footer.html -->
      <?php include 'footer.php' ?>
      <!-- partial -->
    </div>




    
    <!-- page content ends -->
  </div>
  <!--page body ends -->
  <!-- SCRIPT LOADING START FORM HERE /////////////-->
  <!-- plugins:js -->
  <script src="../vendors/js/core.js"></script>
  <script src="../vendors/js/vendor.addons.js"></script>
  <!-- endinject -->
  <!-- Vendor Js For This Page Ends-->
  <script src="../vendors/chartjs/Chart.min.js"></script>
  <!-- Vendor Js For This Page Ends-->
  <script src="../js/script.js"></script>
</body>
</html>