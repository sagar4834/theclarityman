<!DOCTYPE html>
<html lang="en">
	<!-- Mirrored from uxcandy.co/ripple/preview/pages/sample-pages/error_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 02 Mar 2019 17:49:40 GMT -->
	<head>
		<meta charset="utf-8">
			<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
				<title>The Clarity Man</title>
				<!-- plugins:css -->
				<link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
					<link rel="stylesheet" href="../vendors/css/vendor.addons.css">
						<!-- endinject -->
						<!-- vendor css for this page -->
						<!-- End vendor css for this page -->
						<!-- inject:css -->
						<link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
							<!-- endinject -->
							<link rel="shortcut icon" href="../images/favicon.ico">
							</head>
							<body>
								<div class="error_page error_1">
									<div class="container inner-wrapper">
										<h1 class="display-1 error-heading">OOPS!</h1>
										<h2 class="error-code">404 - Page Not Found</h2>
										<p class="error-message">The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
										<a href="#" class="btn btn-outline-primary">Back to Home</a>
									</div>
								</div>
								<!--page body ends -->
								<!-- SCRIPT LOADING START FORM HERE /////////////-->
								<!-- plugins:js -->
								<script src="../vendors/js/core.js"></script>
								<script src="../vendors/js/vendor.addons.js"></script>
								<!-- endinject -->
								<!-- Vendor Js For This Page Ends-->
								<!-- Vendor Js For This Page Ends-->
								<script src="../js/script.js"></script>
							</body>
							<!-- Mirrored from uxcandy.co/ripple/preview/pages/sample-pages/error_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 02 Mar 2019 17:49:40 GMT -->
						</html>