<div class="sidebar">
  <ul class="navigation-menu">
    <li class="nav-category-divider">MAIN</li>
    <li>
      <a href="dashboard.php">
        <span class="link-title">Dashboard</span>
        <i class="mdi mdi-gauge link-icon"></i>
      </a>
    </li>




    <li>
      <a href="#Category" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Category</span>
        <i class="mdi mdi-airplay link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="Category">
        <li>
          <a href="add_category.php">
            <span class="link-title">Add Category</span>
          </a>
        </li>
        <li>
          <a href="all_category.php">
            <span class="link-title">All Category</span>
          </a>
        </li>
      </ul>
    </li>



    <li>
      <a href="#Courses" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Courses</span>
        <i class="mdi mdi-airplay link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="Courses">
        <li>
          <a href="add_courses.php">
            <span class="link-title">Add Courses</span>
          </a>
        </li>
        <li>
          <a href="all_courses.php">
            <span class="link-title">All Courses</span>
          </a>
        </li>
      </ul>
    </li>




    
<?php
fn_mysqli_connect();
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
$sql="SELECT * FROM courses_list";
$result = $con->query($sql);


echo "<li>";
while($row = mysqli_fetch_array($result))
{

  echo "<a href='#Courses_list' data-toggle='collapse' aria-expanded='false'><span class='link-title'>" . $row['category'] . "</span><i class='mdi mdi-airplay link-icon'></i></a><ul class='collapse navigation-submenu' id='Courses_list'>";

  echo "<li><a href='add_courses.php'><span class='link-title'>" . $row['title'] . "</span></a>
</li></ul>";
}
echo "</li>";

mysqli_close($con);
?> 







    <li>
      <a href="#Subscribers" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Subscribers</span>
        <i class="mdi mdi-account-multiple-outline link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="Subscribers">
        <li>
          <a href="all_subscribers.php">
            <span class="link-title">All Subscribers</span>
          </a>
        </li>
      </ul>
    </li>
    
    











  </ul>
  <div class="sidebar_footer">
    <div class="user-account">
      <!-- <div class="user-profile-item-tittle">Switch User</div> -->
      
      <a class="user-profile-item" href="#">
        <i class="mdi mdi-account"></i> Profile
      </a>
      <a class="btn btn-primary btn-logout" href="logout.php">Logout</a>
    </div>
    <div class="btn-group admin-access-level">
      <div class="avatar">
        <i class="mdi mdi-account-circle"></i>
      </div>
      <div class="user-type-wrapper">
        <p class="user_name">
          <?php 
          echo $_SESSION['username'];
          ?>
        </p>
        <div class="d-flex align-items-center">
          <div class="status-indicator small rounded-indicator bg-success"></div>
          <small class="user_access_level">Admin</small>
        </div>
      </div>
      <i class="arrow mdi mdi-chevron-right"></i>
    </div>
  </div>
</div>