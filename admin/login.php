<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
	<title>The Clarity Man</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
	<link rel="stylesheet" href="../vendors/css/vendor.addons.css">
	<!-- endinject -->
	<!-- vendor css for this page -->
	<!-- End vendor css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="../images/favicon.ico">
</head>
<body>
	<div class="authentication-theme auth-style_2">
		<div class="row inner-wrapper">
			<div class="col-md-7 banner-section">
				<div class="auth_footer">
					<p class="text-muted text-center">© The Clarity Man 2019</p>
				</div>
			</div>
			<div class="col-md-5 form-section">
				<div class="logo-section">
					<a href="#" class="logo">
						<img class="logo" src="../images/logo.png" alt="">
					</a>
				</div>



	

				<form action="login_action.php" method="POST">
					<div class="form-group input-rounded">
						<input type="text" name="username" class="form-control" placeholder="username" required="required">
					</div>
					<div class="form-group input-rounded">
						<input type="password" name="password" class="form-control" placeholder="Password" required="required">
					</div>
					<div class="form-inline">
						<div class="checkbox">
							<label>
								<input type="checkbox" class="form-check-input">Remember me 
								<i class="input-frame"></i>
							</label>
						</div>
					</div>
					<button type="submit" name="sub" class="btn btn-primary btn-block">Login</button>
				</form>





				<div class="signup-link">
					<p>Don't have an account yet?</p>
					<a href="registration.php">Register Now</a>
				</div>
			</div>
		</div>
	</div>
	<!--page body ends -->
	<!-- SCRIPT LOADING START FORM HERE /////////////-->
	<!-- plugins:js -->
	<script src="../vendors/js/core.js"></script>
	<script src="../vendors/js/vendor.addons.js"></script>
	<!-- endinject -->
	<!-- Vendor Js For This Page Ends-->
	<!-- Vendor Js For This Page Ends-->
	<script src="../js/script.js"></script>
</body>

</html>