-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2019 at 01:59 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `theclarityman`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_data`
--

CREATE TABLE `admin_user_data` (
  `id` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_user_data`
--

INSERT INTO `admin_user_data` (`id`, `username`, `password`) VALUES
(1, 'admin', 'Admin@123456');

-- --------------------------------------------------------

--
-- Table structure for table `category_list`
--

CREATE TABLE `category_list` (
  `id` int(50) NOT NULL,
  `category` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_list`
--

INSERT INTO `category_list` (`id`, `category`) VALUES
(1, 'Personality Development'),
(2, 'Website Designing'),
(3, 'Photoshop');

-- --------------------------------------------------------

--
-- Table structure for table `courses_list`
--

CREATE TABLE `courses_list` (
  `id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `video1` varchar(100) NOT NULL,
  `video2` varchar(100) NOT NULL,
  `video3` varchar(100) NOT NULL,
  `image1` varchar(100) NOT NULL,
  `image2` varchar(100) NOT NULL,
  `image3` varchar(100) NOT NULL,
  `pdf1` varchar(100) NOT NULL,
  `pdf2` varchar(100) NOT NULL,
  `pdf3` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses_list`
--

INSERT INTO `courses_list` (`id`, `title`, `category`, `video1`, `video2`, `video3`, `image1`, `image2`, `image3`, `pdf1`, `pdf2`, `pdf3`, `description`) VALUES
(1, 'Sagar', 'Personality Development', 'https://www.youtube.com/embed/8367ETnagHo', 'https://www.youtube.com/embed/8367ETnagHo', 'https://www.youtube.com/embed/8367ETnagHo', '1.jpg', '2.jpg', '3.jpg', 'png2pdf.pdf', 'png2pdf.pdf', 'png2pdf.pdf', 'hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  hfksdfs fsjfs sfkhsfk  ');

-- --------------------------------------------------------

--
-- Table structure for table `user_signup`
--

CREATE TABLE `user_signup` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `subscriber_status` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `profession` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_signup`
--

INSERT INTO `user_signup` (`id`, `name`, `subscriber_status`, `mobile`, `email`, `profession`, `password`) VALUES
(1, 'name', 'a', 'mobile', 'email', 'profession', 'password'),
(2, 'Sagar', 'a', '9004998995', 'sagar.pawar68@gmail.com', 'Web Designer', 'Admin@123456'),
(3, 'Siddhesh', 's', '9930089020', 'siddeshpawar@gmail.com', 'Web Designer', 'Admin@123'),
(4, 'Sahil', 's', '9768539850', 'sahil@gmail.com', 'Teacher', 'zxcvbnm123'),
(5, 'Santosh', 'a', '9373501199', 'santosh@gmail.com', 'Teacher', 'Admin@123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user_data`
--
ALTER TABLE `admin_user_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_list`
--
ALTER TABLE `category_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses_list`
--
ALTER TABLE `courses_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_signup`
--
ALTER TABLE `user_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user_data`
--
ALTER TABLE `admin_user_data`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category_list`
--
ALTER TABLE `category_list`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `courses_list`
--
ALTER TABLE `courses_list`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_signup`
--
ALTER TABLE `user_signup`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
