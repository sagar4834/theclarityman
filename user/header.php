<div class="sidebar">
  <ul class="navigation-menu">
    <li class="nav-category-divider">MAIN</li>
    <li>
      <a href="dashboard.php">
        <span class="link-title">Dashboard</span>
        <i class="mdi mdi-gauge link-icon"></i>
      </a>
    </li>




    <li>
      <a href="#Courses" data-toggle="collapse" aria-expanded="false">
        <span class="link-title">Main Courses</span>
        <i class="mdi mdi-airplay link-icon"></i>
      </a>
      <ul class="collapse navigation-submenu" id="Courses">
        <li>
          <a href="course.php">
            <span class="link-title">Sub Courses</span>
          </a>
        </li>
        <li>
          <a href="course.php">
            <span class="link-title">Sub Courses</span>
          </a>
        </li>
        <li>
          <a href="course.php">
            <span class="link-title">Sub Courses</span>
          </a>
        </li>
        <li>
          <a href="course.php">
            <span class="link-title">Sub Courses</span>
          </a>
        </li>
      </ul>
    </li>











  </ul>
  <div class="sidebar_footer">
    <div class="user-account">
      <!-- <div class="user-profile-item-tittle">Switch User</div> -->

      <a class="user-profile-item" href="#">
        <i class="mdi mdi-account"></i> Profile
      </a>
      <a class="btn btn-primary btn-logout" href="logout.php">Logout</a>
    </div>
    <div class="btn-group admin-access-level">
      <div class="avatar">
        <i class="mdi mdi-account-circle"></i>
      </div>
      <div class="user-type-wrapper">
        <p class="user_name">
          <?php 
          echo $_SESSION['email'];
          ?>
        </p>
        <div class="d-flex align-items-center">
          <div class="status-indicator small rounded-indicator bg-success"></div>
          <small class="user_access_level">My Account</small>
        </div>
      </div>
      <i class="arrow mdi mdi-chevron-right"></i>
    </div>
  </div>
</div>