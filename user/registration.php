<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
	<title>The Clarity Man</title>
	<!-- plugins:css -->
	<link rel="stylesheet" href="../vendors/iconfonts/mdi/css/materialdesignicons.css">
	<link rel="stylesheet" href="../vendors/css/vendor.addons.css">
	<!-- endinject -->
	<!-- vendor css for this page -->
	<!-- End vendor css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/theme.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="../images/favicon.ico">
</head>
<body>
	<div class="authentication-theme auth-style_2">
		<div class="row inner-wrapper">
			<div class="col-md-7 banner-section">
				<div class="auth_footer">
					<p class="text-muted text-center">© The Clarity Man 2019</p>
				</div>
			</div>
			<div class="col-md-5 form-section">
				<div class="logo-section">
					<a href="index.html" class="logo">
						<img class="logo" src="../images/logo.png" alt="">
					</a>
				</div>
				<form action="signup_action.php" method="post">


					<div class="form-group input-rounded">
						<input type="text" name="name" class="form-control" placeholder="Your Name" required="required">
					</div>
					<div class="form-group input-rounded">
						<input type="text" name="mobile" class="form-control" placeholder="Your Mobile" required="required">
					</div>
					
					<div class="form-group input-rounded">
						<input type="text" name="profession" class="form-control" placeholder="Profession" required="required">
					</div>
					
					<div class="form-group input-rounded">
						<input type="text" name="email" class="form-control" placeholder="Your Email Address" required="required">
					</div>

					<div class="form-group input-rounded">
						<input type="password" name="password" class="form-control" placeholder="Password" required="required">
					</div>
					
					


					<div class="form-inline">
						<button type="submit" name="submit" class="btn btn-primary btn-block">
							Create Account
						</button>
					</div>
				</form>
				<div class="signup-link">
					<p>Already i have account</p>
					<a href="login.php">Login Now</a>
				</div>
			</div>
		</div>
	</div>
	<!--page body ends -->
	<!-- SCRIPT LOADING START FORM HERE /////////////-->
	<!-- plugins:js -->
	<script src="../vendors/js/core.js"></script>
	<script src="../vendors/js/vendor.addons.js"></script>
	<!-- endinject -->
	<!-- Vendor Js For This Page Ends-->
	<!-- Vendor Js For This Page Ends-->
	<script src="../js/script.js"></script>
</body>

</html>